const express  = require('express');
const { User } = require('./../../models/user');

const router = express.Router();

/*
 *  GET / render page show all users 
 */
router.get('/', (req, res) => {
    let query = {
        type: { $gte: 2 }
    };
    if (req.query.search) {
        const regex = new RegExp(escapeRegex(req.query.search), 'gi');
        query.username = regex;
    }
    User.find(query).then(users => {
        res.render('admin/manage_user', { users });
    }, err => {
        console.log(err);
        res.redirect('back');
    });
});

function escapeRegex(text) {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
};

/*
 *  GET /info/:id xem thong tin chi tiet user 
 */
router.get('/info/:id', (req, res) => {
    User.findById(req.params.id)
        .then(user => {
            if (!user) {
                req.flash('error', 'user không tồn tại!');
                return res.redirect('back');
            }

            res.render('admin/manage_user_detail', { user });
        })
        .catch(err => {
            console.log(err);
            res.redirect('back');
        })
});

/*
 *  POST /change_type/:id change type of a user 
 */
router.put('/change_type/:id', (req, res) => {
    User.findByIdAndUpdate(req.params.id, { $set: { type: req.body.type } }).then(result => {
        res.redirect('back');
    }).catch(err => {
        req.flash('error', err.message);
        res.redirect('back');
    });
});

module.exports = router;